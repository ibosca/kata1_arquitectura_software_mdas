package com.ibosca.kata1;

import org.testng.Assert;
import org.testng.annotations.Test;

public class StringCalculatorTest {

    @Test
    public void add_empty_zero(){
        StringCalculator sut = new StringCalculator();
        int actual = sut.add("");
        int expected = 0;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void add_oneNumber_sameNumber(){
        StringCalculator sut = new StringCalculator();
        int actual = sut.add("5");
        int expected = 5;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void add_twoNumbers_addResult(){
        StringCalculator sut = new StringCalculator();
        int actual = sut.add("5,1");
        int expected = 6;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void add_threeNumbers_addResult(){
        StringCalculator sut = new StringCalculator();
        int actual = sut.add("5,1,2");
        int expected = 8;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void add_numberWithDifferentSeparators_addResult(){
        StringCalculator sut = new StringCalculator();
        int actual = sut.add("1\n2,3");
        int expected = 6;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void add_numberWithOptionalDelimiter_addResult(){
        StringCalculator sut = new StringCalculator();
        int actual = sut.add("//;\n1;2");
        int expected = 3;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void getDelimiter_string_delimiter(){
        StringCalculator sut = new StringCalculator();
        String actual = sut.getDelimiter("//;\n1;2");
        String expected = ";";
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void removeOptionalDelimiter_string_stringWithoutDelimiter(){
        StringCalculator sut = new StringCalculator();
        String actual = sut.removeOptionalDelimiter("//;\n1;2");
        String expected = "1;2";
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void hasOptionalDelimiter_string_booleanTrue(){
        StringCalculator sut = new StringCalculator();
        Boolean actual = sut.hasOptionalDelimiter("//;\n1;2");
        Boolean expected = true;
        Assert.assertEquals(actual, expected);
    }

    @Test
    public void hasOptionalDelimiter_string_booleanFalse(){
        StringCalculator sut = new StringCalculator();
        Boolean actual = sut.hasOptionalDelimiter("**;\n1;2");
        Boolean expected = false;
        Assert.assertEquals(actual, expected);
    }
}
