package com.ibosca.kata1;

class StringCalculator {

    int add(String numbers) {

        int sum = 0;
        String delimiter = getDelimiter(numbers);
        numbers = removeOptionalDelimiter(numbers);

        if(numbers.isEmpty()){
            return sum;
        }

        String[] parts = numbers.split(delimiter);

        for(String number:parts){
            int num = Integer.parseInt(number);
            sum += num;
        }

        return sum;

    }

    String getDelimiter(String numbers){

        String delimiter = "\\n|[,]";

        if(hasOptionalDelimiter(numbers)){
            delimiter = numbers.substring(2, numbers.indexOf("\n"));
        }

        return delimiter;
    }

    String removeOptionalDelimiter(String numbers){

        if(hasOptionalDelimiter(numbers)){
            numbers = numbers.substring(numbers.indexOf("\n")+1);
        }

        return numbers;
    }

    Boolean hasOptionalDelimiter(String numbers){
        return numbers.startsWith("//");
    }
}
